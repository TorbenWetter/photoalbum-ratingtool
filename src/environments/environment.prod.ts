const url = 'https://album.wetter.codes';

export const environment = {
  production: true,
  googleIssuerUrl: 'https://accounts.google.com',
  googleClientId: '200878382006-7jt916cl4k43qe5sb8e1konqo4n8g34d.apps.googleusercontent.com',
  googleScope: 'openid profile email',
  googlePhotosScope: 'https://www.googleapis.com/auth/photoslibrary.readonly',
  photosUrl: url + '/photos',
  apiUrl: url + '/api'
};
