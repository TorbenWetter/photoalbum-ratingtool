// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const url = 'http://localhost:4300';

export const environment = {
  production: false,
  googleIssuerUrl: 'https://accounts.google.com',
  googleClientId: '200878382006-7jt916cl4k43qe5sb8e1konqo4n8g34d.apps.googleusercontent.com',
  googleScope: 'openid profile email',
  googlePhotosScope: 'https://www.googleapis.com/auth/photoslibrary.readonly',
  photosUrl: url + '/photos',
  apiUrl: url + '/api'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
