import { OAuthService } from 'angular-oauth2-oidc';
import { HttpClient } from '@angular/common/http';
import { JwksValidationHandler } from 'angular-oauth2-oidc';

import { Component } from '@angular/core';

import { environment } from 'src/environments/environment';

const maxAlbumPhotoSize = 200; // is also set in .less file

const getPhotoAlbumsUrl = 'https://photoslibrary.googleapis.com/v1/albums?pageSize=50&access_token=';
const getPhotoAlbumContentUrl = 'https://photoslibrary.googleapis.com/v1/mediaItems:search';

type AlbumsResponse = {
  albums: Array<Album>,
  nextPageToken: string
}

type Album = {
  id: string,
  title: string,
  productUrl: string,
  isWriteable: boolean,
  shareInfo: object,
  mediaItemsCount: string,
  coverPhotoBaseUrl: string,
  coverPhotoMediaItemId: string
}

type AlbumAddResponse = {
  success: boolean
}

type AlbumContentResponse = {
  mediaItems: Array<MediaItem>,
  nextPageToken: string
}

type MediaItem = {
  id: string,
  description: string,
  productUrl: string,
  baseUrl: string,
  mimeType: string,
  mediaMetadata: object,
  contributorInfo: object,
  filename: string
}

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.less']
})

export class CreateComponent {
  public albums: Array<Album> = [];
  public maxPhotoAmount: number = 10;

  constructor(private oauthService: OAuthService, private http: HttpClient) {
    this.configure();
  }

  private configure() {
    this.oauthService.configure({
      issuer: environment.googleIssuerUrl,
      redirectUri: window.location.origin,
      clientId: environment.googleClientId,
      scope: environment.googleScope + ' ' + environment.googlePhotosScope,
      strictDiscoveryDocumentValidation: false
    });
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndLogin().then(() => {
      const grantedScopes = this.oauthService.getGrantedScopes()[0];
      if (!grantedScopes.includes(environment.googlePhotosScope)) {
        console.log('No permission for Google Photos');
        return;
      }

      if (!this.oauthService.hasValidAccessToken()) {
        console.log('No valid access token');
        return;
      }

      this.loadAlbums();
    });
  }

  public login() {
    this.oauthService.initLoginFlow();
  }

  public logout() {
    this.oauthService.logOut();
  }

  private requestAlbums(accessToken: string, nextPageToken?: string) {
    const url = getPhotoAlbumsUrl + accessToken + (nextPageToken ? '&pageToken=' + nextPageToken : '');
    this.http.get<AlbumsResponse>(url).subscribe(
      (response: AlbumsResponse) => {
        this.albums = this.albums.concat(response.albums);
        if (response.nextPageToken) {
          this.requestAlbums(accessToken, response.nextPageToken);
        }
      },
      () => alert('Es gab einen Fehler beim Laden der Alben.')
    );
  }

  public loadAlbums() {
    const accessToken = this.oauthService.getAccessToken();
    this.requestAlbums(accessToken);
  }

  public get name() {
    const claims = this.oauthService.getIdentityClaims();
    return claims ? (claims as any).given_name : null;
  }

  public getAlbumUrl(album: Album): string {
    return album.coverPhotoBaseUrl + '=w' + maxAlbumPhotoSize + '-h' + maxAlbumPhotoSize;
  }

  private requestAlbumContent(accessToken: string, albumId: string, albumContent: Array<MediaItem>, nextPageToken?: string) {
    const body = {
      pageSize: '100',
      albumId
    };
    if (nextPageToken) {
      body['pageToken'] = nextPageToken;
    }
    this.http.post<AlbumContentResponse>(getPhotoAlbumContentUrl, body, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + accessToken
      }
    }).subscribe(
      (response: AlbumContentResponse) => {
        albumContent = albumContent.concat(response.mediaItems);
        if (response.nextPageToken) {
          this.requestAlbumContent(accessToken, albumId, albumContent, response.nextPageToken);
        } else { // got complete albumContent
          this.http.post<AlbumAddResponse>(environment.apiUrl + '/add', JSON.stringify({
            albumId: albumId,
            photos: albumContent.map((mediaItem: MediaItem) => {
              return {
                url: mediaItem.baseUrl,
                filename: mediaItem.filename
              };
            }),
            maxPhotoAmount: this.maxPhotoAmount
          })).subscribe(
            (response: AlbumAddResponse) => {
              if (response.success) {
                window.location.href = './' + albumId;
              } else {
                alert('Es gab einen Fehler beim Hinzufügen des Albums.');
              }
            },
            () => alert('Es gab einen Fehler beim Hinzufügen des Albums.')
          );
        }
      },
      () => alert('Es gab einen Fehler beim Laden des Albums.')
    );
  }

  public selectAlbum(album: Album) {
    const accessToken = this.oauthService.getAccessToken();
    this.requestAlbumContent(accessToken, album.id, []);
  }
}
